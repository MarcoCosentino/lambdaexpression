/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.lambdaexpression;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Marco
 */
public class lambda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<Integer>();
        for(int i=1; i<= 5; i++)
            a.add(i);
        stampa(a);
        
        //Per indicare metodo statico di una classe lambda::METODO oppure si può utilizzare una lambda expression
        List<Integer> b= map(a, n -> 2*n); //LAMBDA EXPRESSION (spesso dette FUNZIONI ANONIME), prende un parametro e ne restituisce il doppio
        stampa(b);
        
        List<Integer> c= map(a, n -> 3*n); //LAMBDA EXPRESSION
        stampa(c);
        
        List<Integer> d= map(a, n -> n*n); //LAMBDA EXPRESSION
        stampa(d);
    }

    //Metodo definito con stessi parametri e valori di ritorno dell'interfaccia
    //doppio,triplo,quadrato
    
    //2 - MODO PER ELIMINARE RIPETIZIONE DEL CODICE
    //4 - In questo modo avremo un unico metodo che usa gli altri, specializzando un parametro che è l'interfaccia funzionale.
    //5 - Rendiamo generico il metodo con funzione map (funzione di ordine superiore, poichè prende come parametro una funzione)
    private static<T1,T2> List<T2> map(List<T1> a, Funzione<T1,T2> g) {
        List<T2> b;
        b= new ArrayList<T2>();
        for(T1 e: a)
            b.add(g.applica(e));
        return b;
    }
    
    private static void stampa(List<Integer> a) {
        //Altro modo per implementare interfacce funzionali in questo caso predefinite
        a.forEach(e -> {
            System.out.print(e + " ");
        });
        System.out.println();
    }
    
}
