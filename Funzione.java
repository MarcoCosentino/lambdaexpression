/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.lambdaexpression;

/**
 *
 * @author Marco
 * 
 * Le interfacce che hanno un solo metodo astratto sono dette INTERFACCE FUNZIONALI.
 * Possono essere implementate da classi con tipi corrispondenti ma anche da altri elementi sintattici.
 * INTERFACCIA FUNZIONALE -> serve a rappresentare un metodo che da uno o più parametri fornisce un valore di ritorno.
 */
public interface Funzione<T1,T2> {
    T2 applica(T1 n);
}
